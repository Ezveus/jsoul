package NSLib;
import NSLib.NSUser;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Date;

/**
 * Class NSSocket
 */
public class NSSocket {

    //
    // Constants
    //
    public enum Error {
	NONE,
	CONNECT,
	LOGIN,
	WRITE
    }

    //
    // Fields
    //
    private Socket sock;
    private BufferedReader inBuf;
    private OutputStream outStream;
    private Error errno;
    private NSUser user;

    //
    // Constructors
    //
    public NSSocket(NSUser user) {
	this.errno = Error.NONE;
	this.user = user;
	try {
	    this.sock = new Socket("ns-server.epita.fr", 4242);
	    this.inBuf = new BufferedReader(new InputStreamReader(this.sock.getInputStream()));
	    this.outStream = this.sock.getOutputStream();
	} catch (UnknownHostException ue) {
	    System.err.println("Error : Server " +
			       "ns-server.epita.fr" +
			       " at port " + 4242 +
			       " can't be reach.");
	    errno = Error.CONNECT;
	} catch (IOException io) {
	    System.err.println(io);
	    errno = Error.CONNECT;
	}
    };
 
    //
    // Methods
    //

    /**
     * Authenticate the user 'login' on the server
     * @param        login
     * @param        password
     * @param        location
     * @param        data
     */
    private Error _authenticate(String login, String password,
				String location, String data) {
	try {
	    String we = inBuf.readLine();
	    System.out.println("Debug : " + we); // DEBUG
	    String[] welcomeTab = we.split(" ");
	    String hashMd5 = welcomeTab[2];
	    String localIP = welcomeTab[3];
	    String localPort = welcomeTab[4];
	    String fromSrvr = hashMd5 + "-" + localIP + "/" +
		localPort + password;
	    String md5Str = Utils.Encrypt.encrypt(fromSrvr,
						  "MD5", "UTF-8");
	    String auth_ag = "auth_ag ext_user none none\n";
	    String ext_user_log = "ext_user_log " + login + " " +
		md5Str + " " + data + " " + location + "\n";
	    String rep;

	    outStream.write(auth_ag.getBytes());
	    rep = inBuf.readLine();
	    System.out.println("Debug : " + rep); // DEBUG
	    outStream.write(ext_user_log.getBytes());
	    System.out.println("Debug : " + ext_user_log); // DEBUG
	    rep = inBuf.readLine();
	    System.out.println("Debug : " + rep); // DEBUG
	    if (rep.contains("identification fail")) {
		errno = Error.LOGIN;
		return Error.LOGIN;
	    }
	    outStream.write("attach".getBytes());
	    this.setStatus("actif");
	} catch (IOException io) {
	    errno = Error.LOGIN;
	    System.err.println(io);
	} catch (java.lang.Exception lng) {
	    errno = Error.LOGIN;
	    System.err.println(lng);
	}
	return Error.NONE;
    }

    /**
     * Authenticate the user on the server
     * @param        password
     */
    public Error authenticate(String password) {
	return this._authenticate(this.user.getLogin(), password,
				  this.user.getLocation(),
				  this.user.getData());
    }

    /**
     * Send a message to 'login'
     * @param        message
     * @param        login
     */
    public void sendMessage(String message, String login) {
    }

    /**
     * Get the status
     * @return        status
     */
    public String getStatus() {
	return this.user.getStatus();
    }

    /**
     * Set the status
     * @param        status
     */
    public void setStatus(String status) {
	long timestamp = System.currentTimeMillis() / 1000;
	String cmdStatus = "user_cmd state " + status + ":" + timestamp;

	try {
	    System.out.println("Debug : " + cmdStatus);
	    outStream.write(cmdStatus.getBytes());
	} catch (IOException io) {
	    errno = Error.WRITE;
	    System.err.println(io);
	}
	this.user.setStatus(status);
    }

    /**
     * Disconnect the user
     */
    public void disconnect() {
	try {
	    outStream.write("exit".getBytes());
	    this.sock.close();
	} catch (IOException io) {
	}
    }

    /**
     * Maintain an active connection
     */
    public Error ping() {
	try {
	    outStream.write("ping".getBytes());
	    System.out.println("Debug : ping");
	} catch (IOException io) {
	    errno = Error.CONNECT;
	    System.err.println(io);
	}
	return Error.NONE;
    }
}
