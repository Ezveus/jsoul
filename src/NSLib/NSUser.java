package NSLib;

/**
 * Class NSUser
 */
public class NSUser {

    //
    // Fields
    //

    private String login;
    private String pseudo;
    private String location;
    private String data;
    private String status;
  
    //
    // Constructors
    //
    public NSUser (String login, String pseudo) {
	this.login = login;
	this.pseudo = pseudo;
	this.location = "Somewhere";
	this.data = "Using JSoul";
	this.status = "none";
    };

    public NSUser (String login, String pseudo, String location, String data) {
	this(login, pseudo);
	this.location = location;
	this.data = data;
    };

    public NSUser (String login, String pseudo, String location, String data, String status) {
	this(login, pseudo, location, data);
	this.status = status;
    };

    //
    // Accessor methods
    //

    /**
     * Get the value of login
     * @return the value of login
     */
    public String getLogin() {
	return login;
    }

    /**
     * Get the value of pseudo
     * @return the value of pseudo
     */
    public String getPseudo() {
	return pseudo;
    }

    /**
     * Get the value of location
     * @return the value of location
     */
    public String getLocation() {
	return location;
    }

    /**
     * Get the value of data
     * @return the value of data
     */
    public String getData() {
	return data;
    }

    /**
     * Get the value of status
     * @return the value of status
     */
    public String getStatus() {
	return status;
    }

    /**
     * Set the value of status
     * @param the value of status
     */
    public void setStatus(String status) {
	this.status = status;
    }
}
