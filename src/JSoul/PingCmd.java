package JSoul;
import NSLib.*;

public class PingCmd implements JSoulCmd {
    public boolean cmd(NSSocket sock, String[] cmd) {
	sock.ping();
	return true;
    }
}
