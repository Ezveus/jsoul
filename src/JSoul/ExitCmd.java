package JSoul;
import NSLib.*;

public class ExitCmd implements JSoulCmd {
    public boolean cmd(NSSocket sock, String[] cmd) {
	sock.disconnect();
	return (false);
    }
}
