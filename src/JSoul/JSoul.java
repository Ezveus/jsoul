package JSoul;
import java.lang.NullPointerException;
import java.util.HashMap;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import NSLib.*;

public class JSoul {
    private static HashMap<String, JSoulCmd> initCmds() {
	HashMap<String, JSoulCmd> cmds = new HashMap<String, JSoulCmd>();

	cmds.put("status", new StatusCmd());
	cmds.put("ping", new PingCmd());
	cmds.put("exit", new ExitCmd());
	return cmds;
    }

    private static void loop(NSSocket sock) {
	HashMap<String, JSoulCmd> cmds = initCmds();
	boolean loop = true;

	while (loop) {
	    int ioExceptNb = 0;
	    System.out.print(">>> ");

	    BufferedReader inBuf = new BufferedReader(new InputStreamReader(System.in));
	    try {
		String[] cmd = inBuf.readLine().split(" ");

		if (cmds.containsKey(cmd[0]) == true)
		    loop = cmds.get(cmd[0]).cmd(sock, cmd);
		else
		    System.err.println("Error : Unknown command " + cmd[0]);
	    } catch (IOException io) {
		ioExceptNb++;
		System.err.println("Error : " + io);

		if (ioExceptNb >= 3)
		    break;
	    } catch (NullPointerException except) {
		System.err.println("");
		loop = cmds.get("exit").cmd(sock, null);
	    }
	}
    }

    public static void main(String[] args) {
	NSUser user = new NSUser("ciappa_m", "Ez",
				 "Somewhere", "JSoul");
	NSSocket sock = new NSSocket(user);

	if (sock.authenticate("nw0_cdoe") !=
	    NSSocket.Error.NONE) {
	    System.err.println("Error : Identification failed");
	    return ;
	}
	loop(sock);
    }
}
