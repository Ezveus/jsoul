package JSoul;
import NSLib.*;

public class StatusCmd implements JSoulCmd {
    public boolean cmd(NSSocket sock, String[] cmd) {
	System.out.print("Debug : ");
	for (int i = 0; i < cmd.length; i++)
	    System.out.print(cmd[i] + " ");
	System.out.print("\n");
	if (cmd.length >= 2 && cmd[1].equals("get"))
	    System.out.println("Status : " + sock.getStatus());
	else if (cmd.length >= 3 && cmd[1].equals("set"))
	    sock.setStatus(cmd[2]);
	else
	    System.out.println("Error : status get | status set STATUS");
	return true;
    }
}
